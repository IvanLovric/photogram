﻿using Photogram.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Photogram
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //  Database.SetInitializer(new PhotogramInitializer()); // filling DB with test data during app starts

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
