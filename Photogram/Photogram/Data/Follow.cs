﻿using Photogram.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Photogram.Data
{
    public class Follow
    {
        public int FollowId { get; set; }

        //id of user to follow
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser UserProfile { get; set; }

        //id of user
        public string FollowerId { get; set; }
        [ForeignKey("FollowerId")]
        public virtual ApplicationUser FollowerProfile { get; set; }
    }
}