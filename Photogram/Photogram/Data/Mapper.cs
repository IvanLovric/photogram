﻿using Photogram.Models;

namespace Photogram.Data
{
    public class Mapper
    {
        internal static PartialImageViewModel ImageLocationMapper(Image image, Location location, ApplicationUser user)
        {
            var result = new PartialImageViewModel();

            result.ImageId = image.ImageId;
            result.Path = image.Path;
            result.DateCreated = image.Date.ToLongDateString();
            if (location != null)
                result.LocationName = location.City + " " + location.County;

            result.UserId = user.Id;
            result.Avatar = user.Avatar;
            result.UserName = user.UserName;

            return result;
        }

        internal static UserProfileViewModel UserProfileMapper(ApplicationUser user)
        {
            var profile = new UserProfileViewModel();

            profile.UserId = user.Id;
            profile.Status = user.Status;
            profile.Avatar = user.Avatar;
            profile.UserName = user.UserName;

            return profile;
        }

        internal static PartialLikeViewModel PartialLikeMapper(Like like, ApplicationUser user)
        {
            var temp = new PartialLikeViewModel();

            temp.LikeId = like.LikeId;
            temp.Time = like.Time.ToString();

            temp.Avatar = user.Avatar;
            temp.UserName = user.UserName;
            temp.UserId = user.Id;

            return temp;
        }

        internal static PartialCommentViewModel PartialCommentMapper(Comment comment, ApplicationUser user)
        {
            var temp = new PartialCommentViewModel();

            temp.CommentId = comment.CommentId;
            temp.Content = comment.Content;
            temp.Time = comment.Time.ToString();

            temp.Id = user.Id;
            temp.Avatar = user.Avatar;
            temp.UserName = user.UserName;

            return temp;
        }

        internal static PartialFollowViewModel PartialFollowMapper(Follow follow, ApplicationUser user)
        {
            var temp = new PartialFollowViewModel();

            temp.FollowId = follow.FollowId;

            temp.Id = user.Id;
            temp.Avatar = user.Avatar;
            temp.UserName = user.UserName;

            return temp;
        }
    }
}