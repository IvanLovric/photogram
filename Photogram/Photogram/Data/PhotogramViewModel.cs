﻿using System.Collections.Generic;

namespace Photogram.Data
{
    public class PartialImageViewModel
    {
        public int ImageId { get; set; }
        public string Path { get; set; }
        public string DateCreated { get; set; }

        public List<PartialCommentViewModel> PartialComments { get; set; }
        public List<PartialLikeViewModel> PartialLikes { get; set; }

        public string LocationName { get; set; }
        public string UserId { get; set; }
        public string Avatar { get; set; }
        public string UserName { get; set; }
    }

    public class UserProfileViewModel
    {
        public string UserId { get; set; }
        public string Status { get; set; }
        public string Avatar { get; set; }
        public string UserName { get; set; }

        public List<PartialImageViewModel> PartialImages { get; set; }

        public List<PartialFollowViewModel> Followers { get; set; }
        public List<PartialFollowViewModel> Followings { get; set; }
    }

    public class PartialLikeViewModel
    {
        public int LikeId { get; set; }
        public string Time { get; set; }

        public string Avatar { get; set; }
        public string UserName { get; set; }

        public string UserId { get; set; }
    }

    public class PartialCommentViewModel
    {
        public int CommentId { get; set; }
        public string Content { get; set; }
        public string Time { get; set; }

        public List<Hashtag> Hashtags { get; set; }

        public string Id { get; set; }
        public string Avatar { get; set; }
        public string UserName { get; set; }
    }

    public class PartialFollowViewModel
    {
        public int FollowId { get; set; }
        public string Id { get; set; }
        public string Avatar { get; set; }
        public string UserName { get; set; }
    }
}