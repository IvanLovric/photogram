﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Photogram.Data
{
    public class Hashtag
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HashtagId { get; set; }

        [Required]
        public string Tag { get; set; }

        [InverseProperty("Hashtags")]
        public virtual ICollection<Comment> Comments { get; set; }
    }
}