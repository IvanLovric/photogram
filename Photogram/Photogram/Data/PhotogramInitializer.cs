﻿using Photogram.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Photogram.Data
{
    public class PhotogramInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            ApplicationUser _user1 = new ApplicationUser()
            {
                UserName = "Ivan Lovrić",
                Avatar = "~/Content/Template/images/user_Ivan.jpg",
                Status = "Carpe diem",
                Email = "ivan.lovric@gmail.com",
                EmailConfirmed = false,
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = false,
                AccessFailedCount = 0
            };

            ApplicationUser _user2 = new ApplicationUser()
            {
                UserName = "Mavro Božičević",
                Avatar = "~/Content/Template/images/user_Mavro.jpg",
                Status = "No status",
                Email = "mbozic00@gmail.com",
                EmailConfirmed = false,
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = false,
                AccessFailedCount = 0
            };

            Image _image1 = new Image()
            {
                Path = "~/Content/Template/images/image1.png",
                Date = DateTime.Now,
                User = _user1
            };

            Image _image2 = new Image()
            {
                Path = "~/Content/Template/images/image2.jpg",
                Date = DateTime.Now,
                User = _user1
            };

            Like _like1 = new Like() { Image = _image1, User = _user1 };
            Like _like2 = new Like() { Image = _image2, User = _user1 };
            Like _like3 = new Like() { Image = _image2, User = _user2 };

            Comment _comment1 = new Comment()
            {
                Content = "Wonderful beach, wonderful picture!",
                Time = DateTime.Now,
                User = _user1,
                Image = _image1,
                Hashtags = new List<Hashtag>()
                {
                    new Hashtag()
                    {
                        Tag = "#beach"
                    },
                    new Hashtag()
                    {
                        Tag = "#sunset"
                    },
                    new Hashtag()
                    {
                        Tag = "#summer"
                    }
                }
            };

            Location _location1 = new Location()
            {
                City = "Paris Eiffel Tower",
                County = "France",

                Images = new List<Image>()
                {
                    new Image()
                    {
                        Path = "~/Content/Template/images/image5.jpg",
                        Date = DateTime.Now,
                        User = _user2
                    }
                }
            };

            Follow _follow1 = new Follow() { UserProfile = _user1, FollowerProfile = _user2 };
            Follow _follow2 = new Follow() { UserProfile = _user2, FollowerProfile = _user1 };

            context.Users.Add(_user1);
            context.Users.Add(_user2);
            context.Images.Add(_image1);
            context.Images.Add(_image2);
            context.Likes.Add(_like1);
            context.Likes.Add(_like2);
            context.Likes.Add(_like3);
            context.Comments.Add(_comment1);
            context.Locations.Add(_location1);
            context.Follows.Add(_follow1);
            context.Follows.Add(_follow2);
            base.Seed(context);
        }
    }
}