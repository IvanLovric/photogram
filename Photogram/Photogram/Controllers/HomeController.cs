﻿using Microsoft.AspNet.Identity;
using Photogram.Data;
using Photogram.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Photogram.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        // GET: All images at home page
        public JsonResult GetHomePage()
        {
            var result = new List<PartialImageViewModel>();
            var _images = db.Images.ToList().OrderByDescending(x => x.Date);

            foreach (var image in _images)
            {
                var imageResult = new PartialImageViewModel();

                //get user
                var _user = db.Users.Where(x => x.Id == image.Id).First();

                //get location (if location exist)
                if (image.LocationId != null)
                {
                    var _location = db.Locations.Where(x => x.LocationId == image.LocationId).First();
                    imageResult = Mapper.ImageLocationMapper(image, _location, _user);
                }
                else
                {
                    imageResult = Mapper.ImageLocationMapper(image, null, _user);
                }

                //get likes 
                imageResult.PartialLikes = new List<PartialLikeViewModel>();
                var _likes = db.Likes.Where(x => x.ImageId == image.ImageId).ToList();

                foreach (var like in _likes)
                {
                    //get user who liked picture
                    var _liker = db.Users.Where(x => x.Id == like.Id).First();

                    var temp = new PartialLikeViewModel();
                    temp = Mapper.PartialLikeMapper(like, _liker);
                    imageResult.PartialLikes.Add(temp);
                }

                //get comments 
                imageResult.PartialComments = new List<PartialCommentViewModel>();
                var _comments = db.Comments.Where(x => x.ImageId == image.ImageId).ToList();

                foreach (var comment in _comments)
                {
                    //get user who commented picture
                    var _commentator = db.Users.Where(x => x.Id == comment.Id).First();

                    var temp = new PartialCommentViewModel();
                    temp = Mapper.PartialCommentMapper(comment, _commentator);

                    //get hashtags (if they exist) 
                    temp.Hashtags = new List<Hashtag>();
                    var _hashtags = db.Hashtags.Where(x => x.Comments.Any(y => y.CommentId == comment.CommentId)).ToList();

                    foreach (var tag in _hashtags)
                    {
                        temp.Hashtags.Add(tag);
                    }
                    imageResult.PartialComments.Add(temp);
                }

                result.Add(imageResult);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: User profile by Id
        public JsonResult GetUserProfile(string id)
        {
            var profile = new UserProfileViewModel();
            var _user = db.Users.Where(x => x.Id == id).First();

            profile = Mapper.UserProfileMapper(_user);

            //get user images 
            profile.PartialImages = new List<PartialImageViewModel>();
            var _images = db.Images.Where(x => x.Id == profile.UserId).ToList().OrderByDescending(x => x.Date); ;

            foreach (var image in _images)
            {
                var temp = new PartialImageViewModel();
                temp = Mapper.ImageLocationMapper(image, null, _user);
                profile.PartialImages.Add(temp);

                //get likes of every image 
                temp.PartialLikes = new List<PartialLikeViewModel>();
                var _likes = db.Likes.Where(x => x.ImageId == image.ImageId).ToList();

                foreach (var like in _likes)
                {
                    var tempLike = new PartialLikeViewModel();
                    tempLike = Mapper.PartialLikeMapper(like, _user);
                    temp.PartialLikes.Add(tempLike);
                }
            }

            //get followers
            profile.Followers = new List<PartialFollowViewModel>();
            var _followers = db.Follows.Where(x => x.UserId == profile.UserId).ToList();

            foreach (var follower in _followers)
            {
                var temp = new PartialFollowViewModel();
                var user = db.Users.Where(x => x.Id == follower.FollowerId).First();
                temp = Mapper.PartialFollowMapper(follower, user);
                profile.Followers.Add(temp);
            }

            //get  followings
            profile.Followings = new List<PartialFollowViewModel>();
            var _followings = db.Follows.Where(x => x.FollowerId == profile.UserId).ToList();

            foreach (var follower in _followings)
            {
                var temp = new PartialFollowViewModel();
                var user = db.Users.Where(x => x.Id == follower.UserId).First();
                temp = Mapper.PartialFollowMapper(follower, user);
                profile.Followings.Add(temp);
            }

            return Json(profile, JsonRequestBehavior.AllowGet);
        }

        // POST: Upload photo to server
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadPhoto(HttpPostedFileBase photo)
        {
            if (ModelState.IsValid)
            {
                if (photo != null)
                {
                    try
                    {
                        var fileName = Path.GetFileName(photo.FileName);
                        var fileSavePath = Server.MapPath("~/Content/Template/images/" + fileName);
                        photo.SaveAs(fileSavePath);

                        Image img = new Image
                        {
                            Path = "Content/Template/images/" + fileName,
                            Date = DateTime.Now,
                            Id = User.Identity.GetUserId()
                        };
                        using (ApplicationDbContext db = new ApplicationDbContext())
                        {
                            db.Images.Add(img);
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                        // TODO: msg "Error while posting a photo";       
                    }
                }
                return RedirectToAction("Index");
            }

            return View();
        }


        // POST: Add comment to image
        [HttpPost]
        [Authorize]
        public string AddComment(string content, int imageId)
        {
            if (content != null)
            {
                var result = new Comment();
                result.Time = DateTime.Now;
                result.Content = content;

                result.ImageId = imageId;
                result.Id = User.Identity.GetUserId();

                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Comments.Add(result);
                    db.SaveChanges();
                    return "Comment added successfully";
                }
            }
            else
            {
                return "Invalid comment record";
            }
        }

        // POST: Add like to image
        [HttpPost]
        [Authorize]
        public string AddLike(int imageId)
        {
            var _like = new Like();
            _like.Time = DateTime.Now;

            _like.ImageId = imageId;
            _like.Id = User.Identity.GetUserId();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                db.Likes.Add(_like);
                db.SaveChanges();
                return "Like added successfully";
            }
        }

        // POST: Add follow to user
        [HttpPost]
        [Authorize]
        public string AddFollow(string userId)
        {
            var _follow = new Follow();
            _follow.UserId = userId;
            _follow.FollowerId = User.Identity.GetUserId();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                db.Follows.Add(_follow);
                db.SaveChanges();
                return "Follow added successfully";
            }
        }

    }
}