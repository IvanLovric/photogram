﻿app.service("PhotogramAppService", function ($http, $routeParams) {

    //GET home page from server
    this.getImages = function () {
        return $http.get("Home/GetHomePage");
    };

    //GET user profile from server
    this.getProfile = function () {
        var id = $routeParams.id;

      return  $http({
            method: 'POST',
            url: 'Home/GetUserProfile',
            data: {
                id: id
            }
        })
    }

    // POST Comment and Image Id to server
    this.AddComment = function (content, id) {

        var response = $http({
            method: "post",
            url: "Home/AddComment",
            data: {
                content: content,
                imageId: id
            }
        });
        return response;
    }

    // POST Like and Image Id to server
    this.AddLike = function (id) {

        var response = $http({
            method: "post",
            url: "Home/AddLike",
            data: {
                imageId: id
            }
        });
        return response;
    }

    //POST Follow to Server
    this.AddFollow = function (userid) {

        var response = $http({
            method: "post",
            url: "Home/AddFollow",
            data: {
                userId: userid
            }
        });
        return response;
    }
});
    