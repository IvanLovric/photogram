﻿var app = angular.module("PhotogramApp", ['ngRoute'])

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
        .when('/', {
            title: 'HomePage',
            templateUrl: '/Content/Template/Home.html',
            controller: 'HomePageController'
        })
        .when('/Profile/:id', {
            title: 'User Page',
            templateUrl: '/Content/Template/Profile.html',
            controller: 'UserProfileController'
            })
        .otherwise({
            redirectTo: 'Main Page'
        });
    }]);