﻿app.controller("HomePageController", function ($scope, PhotogramAppService) {

    GetHomePage();
    //To Get all image records  
    function GetHomePage() {
        var getImageData = PhotogramAppService.getImages();
        getImageData.then(function (img) {
            //Successful
            $scope.images = img.data;
        }, function () {
            //Error
            alert('Error in getting images records');
        });
    }

    //Add comment to image
    $scope.AddUpdateComment = function (commentContent, index) {
        var comment = commentContent;
        var id = $scope.images[index].ImageId;

        var getCommentData = PhotogramAppService.AddComment(comment, id);
        getCommentData.then(function (msg) {
            //Successful
                GetHomePage();
                alert(msg.data);
        }, function () {
            //Error
                alert('Error in adding comment record');
            });
    }

    //Add like to image
    $scope.AddLike = function (index) {
        var id = $scope.images[index].ImageId;

        var getLikeData = PhotogramAppService.AddLike(id);
        getLikeData.then(function (msg) {
            //Successful
            GetHomePage();
            alert(msg.data);
        }, function () {
            //Error
            alert('Error in liking picture.');
        });
    }

});

app.controller("UserProfileController", function ($scope, PhotogramAppService) {

    GetProfilePage();
    //To Get user profile page 
    function GetProfilePage() {
        var getProfileData = PhotogramAppService.getProfile();
        getProfileData.then(function (response) {
            //Successful
            $scope.profile = response.data;
        }, function () {
            //Error
            alert('Error in getting profiles records');
        });
    }

    //Add follow to user
    $scope.AddFollow = function (index) {
        var id = $scope.profile.UserId;

        var getFollowData = PhotogramAppService.AddFollow(id);
        getFollowData.then(function (msg) {
            //Successful
            GetProfilePage();
            alert(msg.data);
        }, function () {
            //Error
            alert('Error in following user.');
        });
    }

});



